using Toybox.WatchUi as Ui;
using Toybox.System as Sys;
using Toybox.Application.Storage as St;

class DemoEzRestMenuDelegate extends Ui.MenuInputDelegate {

    function initialize() {
        MenuInputDelegate.initialize();
    }

    function onMenuItem(item) {
        if (item == :sync) {
        	St.clearValues();
        	St.setValue("lastsync", timestamp);
            Sys.println("Dados Apagados");
        } else if (item == :item_2) {
            Ui.requestUpdate();
        }
    }

}
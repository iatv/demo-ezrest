using Toybox.WatchUi as Ui;

class DemoEzRestDelegate extends Ui.BehaviorDelegate {

    function initialize() {
        BehaviorDelegate.initialize();
    }

    function onMenu() {
        Ui.pushView(new Rez.Menus.MainMenu(), new DemoEzRestMenuDelegate(), Ui.SLIDE_UP);
        return true;
    }

}
using Toybox.Application as App;
using Toybox.WatchUi as Ui;
using Toybox.Background as Bg;
using Toybox.Sensor;
using Toybox.System;
using Toybox.Time;

var ezrest = new Actigraphy();
var timestamp = Time.now().value();
var accelX = [];
var accelY = [];
var accelZ = [];
var pitch = [];
var power = [];
var roll = [];

var counter=0;
var bgdata="none";
var OSCOUNTER="oscounter";
var OSDATA="osdata";

(:background)
class DemoEzRestApp extends App.AppBase {

    function initialize() {
        AppBase.initialize();
        App.Storage.clearValues();
		App.Storage.setValue("lastsync", timestamp);
    }

    // onStart() is called on application start up
    function onStart(state) {
    	ezrest.enableAccel();
    }

    // onStop() is called when your application is exiting
    function onStop(state) {
    }

    // Return the initial view of your application here
    function getInitialView() {
    	if(Toybox.System has :ServiceDelegate) {
    		Background.registerForTemporalEvent(Time.now().add(new Time.Duration(10)));
    	} else {
    		Sys.println("****background not available on this device****");
    	}
        return [ new DemoEzRestView(), new DemoEzRestDelegate() ];
    }

    function onBackgroundData(data) {
    	counter++;
    	var now=System.getClockTime();
    	var ts=now.hour+":"+now.min.format("%02d");
        System.println("onBackgroundData="+data+" "+counter+" at "+ts);
        bgdata=data;
        App.getApp().setProperty(OSDATA,bgdata);
		Ui.requestUpdate();
    }

    function getServiceDelegate(){
    	var now=System.getClockTime();
    	var ts=now.hour+":"+now.min.format("%02d");    
    	System.println("getServiceDelegate: "+ts);
        return [new BgServiceDelegate()];
    }

}



class Actigraphy {
    function enableAccel() {
        var maxSampleRate = Sensor.getMaxSampleRate();

        // acelerometro lido a cada um segundo
        var options = {:period => 1, :sampleRate => maxSampleRate, :enableAccelerometer => true, :includePower => true, :includePitch => true, :includeRoll => true};
        try {
            Sensor.registerSensorDataListener(method(:accelHistoryCallback), options);
        }
        catch(e) {
            System.println(e.getErrorMessage());
        }
    }

    function accelHistoryCallback(sensorData) {
        accelX.addAll(sensorData.accelerometerData.x);
        accelY.addAll(sensorData.accelerometerData.y);
        accelZ.addAll(sensorData.accelerometerData.z);
        pitch.addAll(sensorData.accelerometerData.pitch);
        power.addAll(sensorData.accelerometerData.power);
        roll.addAll(sensorData.accelerometerData.roll);
                
        if (accelX.size() >= 1) {
        	var minutedata = [
	        	filterData(accelX),
	        	filterData(accelY),
	        	filterData(accelZ),
	        	filterData(pitch),
	        	filterData(power),
	        	filterData(roll)
        	];
        	
        	saveData(minutedata);
        }
        
        Ui.requestUpdate();
    }
    
    function filterData(list) {
    	//fazendo uma média temporariamente até termos algoritmo de filtragem
    	var sum = 0;
    	for( var i = 0; i < list.size(); i++ ) {
    		sum = sum + list[i];
    	}

    	return sum / list.size();
    
    }
    
    function countData(string) {
    	var lastsync = App.Storage.getValue("lastsync");
    	var count = 0;
    	for (var i = lastsync; i < timestamp; i++ ) {
    		if ( App.Storage.getValue(i) != null ) {
    			count++;
    		}
    	}
    	if (string) {
    		return count.toString();
    	} else {
    		return count;
    	}
    }
    
    function saveData(data) {
    	timestamp = Time.now().value();

		try {
			App.Storage.setValue(timestamp, data);
			if (App.Storage.getValue("lastsync") == 0) {
				App.Storage.setValue("lastsync", timestamp);
			}
    	} catch (err) {
    		System.println("storage limit");
    	}

		accelX = [];
		accelY = [];
		accelZ = [];
		pitch = [];
		power = [];
		roll = [];
    }

    function disableAccel() {
        Sensor.unregisterSensorDataListener();
    }
}


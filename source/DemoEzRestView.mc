using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;

class DemoEzRestView extends Ui.View {

	hidden var labelT;
	hidden var labelD;

    function initialize() {
        View.initialize();
    }

    // Load your resources here
    function onLayout(dc) {
        setLayout(Rez.Layouts.MainLayout(dc));
        labelT = View.findDrawableById("timestamp");
        labelD = View.findDrawableById("dataSize");
    }

    // Called when this View is brought to the foreground. Restore
    // the state of this View and prepare it to be shown. This includes
    // loading resources into memory.
    function onShow() {
    }

    // Update the view
    function onUpdate(dc) {
        // Call the parent onUpdate function to redraw the layout
        View.onUpdate(dc);

		labelT.setText(timestamp.toString());
		labelD.setText(Actigraphy.countData(true));

    }

    // Called when this View is removed from the screen. Save the
    // state of this View here. This includes freeing resources from
    // memory.
    function onHide() {
    }

}
